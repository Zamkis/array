# Tableau dynamique

Implémentation de base en C d'un tableau redimensionnable.

## Fonctions supportées

Les fonctions implémentées actuellement sont :

```c
Array arrayCreate();
void arrayInsert(Array *a, int element);
void arrayRemove(Array *a, int i);
bool arrayHasElement(const Array *a, int element);
int arrayGet(const Array *a, int i);
void arrayDelete(Array *a);
```

## Fonctionnement

Il suffit de taper
```
make
```
pour compiler. Ensuite, on exécute le programme avec la commande
```
./array
```

## Auteur

Alexandre Blondin Massé
